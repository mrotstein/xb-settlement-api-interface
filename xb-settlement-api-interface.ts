// Backend entities:
// - Configuration (corridor, steps, CRUD)
// - Flow (preview, execution)
// - Step: ExchagneStep, VaultStep
// Assumptions:
// - Corridors and their steps should be hard-coded for now

// -------------- Configuration Routes --------------

// Creating a new configuration:
// endpoint: POST /xb-settlements/configs
// body:
interface XBCreateConfigurationRequest {
    corridorId: string; // for now - only 'MX_US'
    name: string; // friendly name
    steps: Record<XBSettlementStepType, XBSettlementConfigStep>
}
// response:
interface XBCreateConfigurationResponse {
    XBSettlementConfigModel
}

// Get a configuration:
// endpoint: GET /xb-settlements/configs/<id>
// body: -
// response:
interface XBGetConfigurationResponse {
    XBSettlementConfigModel
}

// Get all configuration under a tenant:
// endpoint: GET /xb-settlements/configs
// body: - (tenant is part of the jwt)
// response:
export type GetAllXBSettlementConfigsResponse = {
    configurations: Array<XBSettlementConfigModel>;
}

// Updating a configuration
// endpoint: PUT /xb-settlements/configs/<id>
// body:
interface XBUpdateConfigurationRequest {
    name: string; // friendly name
    steps: Record<XBSettlementStepType, XBSettlementStepData>
}
// response:
interface XBUpdateConfigurationResponse {
    XBSettlementConfigModel
}

// Deleting a configuration
// endpoint DELETE /xb-settlements/configs/<id>
// response:
interface XBDeleteConfigurationResponse {
    configId: string;
}


// -------------- Flow Routes --------------
// Creating a flow (preview request)
// POST /xb-settlements/flows
// request: 
export interface XBCreateSettlementFlowRequest {
    configId: string;
    amount: { amount: string; assetId: string }; 
}
// response: 
export interface XBCreateSettlementFlowResponse {
    XBGetSettlementFlowSetup
}



// ------------- <DONT IMPLEMENT YET> --------------
// Updating a flow (updated flow preview request) 
// PUT /xb-settlements/flows/<id>  
// ------------- </DONT IMPLEMENT YET> --------------



// Executing a flow (launching a payment)
// POST /xb-settlements/flows/<flow-id>/actions/execute
// request: 
// body: -
// response:
export interface XBSettlementFlowExecutionResponse {
    XBSettlementFlowExecution
}
  
// Getting a flow (in setup (preview) mode, or execution mode)
// GET /xb-settlements/flows/<flow-id>
// request: 
// body: -
// response:
export interface XBGetSettlementFlowExecutionResponse {
    setup?: XBGetSettlementFlowSetup; // will be defined if the flow is in preview mode
    execution?: XBSettlementFlowExecution; // will be defined in the flow is in execution mode
}


// --------------------------------------
// --------------------------------------

export interface XBSettlementConfigModel {
    configId: string;
    corridorId: string;
    name: string; // friendly name
    steps: Record<XBSettlementStepType, XBSettlementConfigStep>
}

export type XBSettlementConfigStep = {
    accountId: string;
}

export enum XBSettlementStepType {
    ON_RAMP = 'ON_RAMP',
    VAULT_ACCOUNT = 'VAULT_ACCOUNT',
    OFF_RAMP = 'OFF_RAMP',
    FIAT_DESTINATION = 'FIAT_DESTINATION',
}
  
export interface XBSettlementFlowStep {
    accountId: string;
    inputAmount: { amount: string; assetId: string }; 
    outputAmount: { amount: string; assetId: string }; 
    estimatedFee: { amount: string; assetId: string };
    estimatedTime: number; // time in seconds
}

enum XBSettlementFlowExecutionStepStatus {
    NOT_STARTED = 'NOT_STARTED',
    PROCESSING = 'PROCESSING',
    COMPLETED = 'COMPLETED',
    FAILED = 'FAILED',
}

export interface XBSettlementFlowExecutionStep {
    id: string;
    accountId: string;
    status: XBSettlementFlowExecutionStepStatus;
    transferAmount: { amount: string; assetId: string };
    fee: { amount: string; assetId: string };
    startedAt: number; // milliseconds UTC
}

export interface XBGetSettlementFlowSetup {
    flowId: string;
    configId: string;
    steps: Record<XBSettlementStepType, XBSettlementFlowStep>;
    inputAmount: { amount: string; assetId: string }; 
    outputAmount: { amount: string; assetId: string }; 
    totalEstimatedFee: string;
    totalEstimatedTime: number; // time in seconds
}

export enum XBSettlementFlowExecutionStatus {
    PROCESSING = 'PROCESSING',
    COMPLETED = 'COMPLETED',
    FAILED = 'FAILED',
}

export interface XBSettlementFlowExecution {
    flowId: string;
    configId: string; 
    steps: Record<XBSettlementStepType, XBSettlementFlowExecutionStep>;
    inputAmount: { amount: string; assetId: string }; // constant through the process of execution (already calulated during setup)
    outputAmount: { amount: string; assetId: string }; // output amount (including fees) so far in the process of execution
    totalFee: { amount: string; assetId: string };
    initiatedAt: number; // milliseconds UTC. (this time should originate from the execution and not preview)
    state: XBSettlementFlowExecutionStatus
}